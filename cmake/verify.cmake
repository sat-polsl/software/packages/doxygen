function(verify RESULT)
    message(DEBUG "Verifying hashes")
    
    if(EXISTS "${ROOT}/.md5")
        file(GLOB_RECURSE FILES LIST_DIRECTORIES FALSE "${ROOT}/**/*")
        list(REMOVE_ITEM FILES "${ROOT}/.md5")

        set(CURRENT_HASH_LIST "")
        foreach(FILE ${FILES})
            set(HASH_VALUE "")
            file(MD5 ${FILE} HASH_VALUE)
            list(APPEND CURRENT_HASH_LIST ${HASH_VALUE})

            message(DEBUG "Hashed ${FILE}: ${HASH_VALUE}")
        endforeach()

        message(DEBUG "Hash result: ${CURRENT_HASH_LIST}")
    
        set(HASH_LIST "") 
        file(READ ${ROOT}/.md5 HASH_LIST)

        message(DEBUG "Readed hashes: ${HASH_LIST}")

        if("${CURRENT_HASH_LIST}" STREQUAL "${HASH_LIST}")
            message(VERBOSE "Hashes verified")
            set(${RESULT} TRUE PARENT_SCOPE)
        else()
            message(VERBOSE "Hashes not verified")
        endif()
    else()
        message(VERBOSE "Hash file not found")
    endif()
endfunction()
