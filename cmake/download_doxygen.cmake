set(DOXYGEN_WINDOWS_URL https://www.doxygen.nl/files/doxygen-${VERSION}.windows.x64.bin.zip)
set(DOXYGEN_LINUX_URL https://www.doxygen.nl/files/doxygen-${VERSION}.linux.bin.tar.gz)

macro(_download_for_linux)
    message(VERBOSE "Downloading Doxygen for Linux from: ${DOXYGEN_LINUX_URL}")

    file(DOWNLOAD ${DOXYGEN_LINUX_URL} ${ROOT}/doxygen.tar.gz)
    file(ARCHIVE_EXTRACT 
         INPUT ${ROOT}/doxygen.tar.gz
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/doxygen.tar.gz)
endmacro()

macro(_download_for_windows)
    message(VERBOSE "Downloading Doxygen for Windows from: ${DOXYGEN_WINDOWS_URL}")

    file(DOWNLOAD ${DOXYGEN_WINDOWS_URL} ${ROOT}/doxygen.zip)
    file(ARCHIVE_EXTRACT 
         INPUT ${ROOT}/doxygen.zip
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/doxygen.zip)
endmacro()

function(_calculate_hash RESULT)
    message(DEBUG "Calculating hashes")
    
    file(GLOB_RECURSE FILES LIST_DIRECTORIES FALSE "${ROOT}/**/*")

    set(HASH_LIST "")
    foreach(FILE ${FILES})
        set(HASH_VALUE "")
        file(MD5 ${FILE} HASH_VALUE)
        list(APPEND HASH_LIST ${HASH_VALUE})

        message(DEBUG "Hashed ${FILE}: ${HASH_VALUE}")
    endforeach()

    message(DEBUG "Hashing result: ${HASH_LIST}")
    set(${RESULT} ${HASH_LIST} PARENT_SCOPE)
endfunction()

macro(download_doxygen)
    if(NOT IS_DIRECTORY ${ROOT})
        file(MAKE_DIRECTORY ${ROOT})
    endif()

    if(WIN32)
        _download_for_windows()
    elseif(UNIX)
        _download_for_linux()
    endif()

    set(HASH "")
    _calculate_hash(HASH)

    file(WRITE ${ROOT}/.md5 "${HASH}")
endmacro()
