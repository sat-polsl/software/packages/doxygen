cmake_minimum_required(VERSION 3.25)
set(ROOT ${CMAKE_CURRENT_SOURCE_DIR}/.download)
set(VERSION 1.9.6)

include(cmake/download_doxygen.cmake)
include(cmake/verify.cmake)

message(STATUS "Doxygen ${VERSION} Package")
find_package(Doxygen ${VERSION} QUIET)

if(NOT Doxygen_FOUND)
    set(VERIFIED FALSE)
    verify(VERIFIED)
    
    if(NOT VERIFIED)
        file(REMOVE_RECURSE ${ROOT})
        download_doxygen()
    endif()
    
    if(WIN32)
        set(Doxygen_ROOT ${ROOT})
    elseif(UNIX)
        set(Doxygen_ROOT ${ROOT}/doxygen-${VERSION}/bin)
    endif()
endif()

find_package(Doxygen ${VERSION} REQUIRED)

